# arato
El bot del grupo de unos amigos.

Utilizamos [`python-telegram-bot`](https://python-telegram-bot.org/) para
conectarnos a telegram y hacer cosillas.

## Instalación y ejecución
Lo primero es tener python y pip instalado. No tengo apt pero supongo que será
algo de este estilo:

```
$ sudo apt install python pip
```

Luego necesitamos instalar la librería que utilizamos para conectarnos con
python a telegram (pip es el gestor de paquetes de python):

```
$ pip install python-telegram-bot
```

Y para ejecutar el bot simplemente:

```
$ python main.py
```